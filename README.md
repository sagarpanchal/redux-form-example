# Setup Instructions

```
npm i   # to install required npm packages
```

### ESLint plugins for vscode

```
npm i -g eslint eslint-plugin-react babel-eslint
```

### NPM scripts

```
npm run watch   # for live updates while testing,
npm run dev     # for development build and
npm run prod    # for production build.
```

### Delete .git in windows

```
cmd> del /F /S /Q /A .git
```
