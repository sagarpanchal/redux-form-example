import regEx from "../libraries/RegEx";

export const validate = values => {
  let errors = {};
  let fields = [
    "firstName",
    "lastName",
    "phone",
    "birthdate",
    "color",
    "email"
  ];

  regEx.firstName = regEx.name;
  regEx.lastName = regEx.name;

  let customError = {
    firstName: "Must be >1 characters",
    lastName: "Must be >1 characters",
    email: "Invalid address",
    phone: "Must be =10 numbers"
  };

  fields.forEach(field => {
    let fieldName = field.replace(
      /(?:^|\.?)([A-Z])/g,
      x => " " + x.toLowerCase()
    );
    fieldName = fieldName.charAt(0).toUpperCase() + fieldName.slice(1);

    errors[field] = !values[field] ? fieldName + " can't be empty" : null;
    if (!errors[field] && regEx[field]) {
      if (!regEx[field].test(values[field])) {
        errors[field] = customError.hasOwnProperty(field)
          ? customError[field]
          : "Invalid " + field;
      }
    }
  });

  return errors;
};

export const asyncValidate = values => {
  return new Promise(resolve => setTimeout(resolve, 3000)).then(() => {
    if (["panchal.sagar@outlook.com"].includes(values.email)) {
      throw { email: "This email is taken" };
    }
  });
};
