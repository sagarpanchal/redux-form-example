import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";

import { connect } from "react-redux";
import { Field, initialize, reduxForm } from "redux-form";
import { getFormMeta, getFormSyncErrors, getFormAsyncErrors } from "redux-form";
import { validate, asyncValidate } from "./ReduxFormValidate";
// import { load } from "../store/actions/index";

import { Card, CardBody, CardHeader } from "reactstrap";
import { Button, Form, FormGroup, Label } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import data from "./FormPreInit";
import ReduxFormViewer from "./ReduxFormViewer";

class ReduxForm extends Component {
  static propTypes = {
    asyncErrorList: PropTypes.object,
    asyncValidating: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    syncErrorList: PropTypes.object,
    handleSubmit: PropTypes.func,
    loadDefaults: PropTypes.func,
    meta: PropTypes.object,
    pristine: PropTypes.bool,
    reset: PropTypes.func,
    submitting: PropTypes.bool
  };

  focusInput = e => {
    let input = document.getElementsByName(e.target.htmlFor)[0];
    input !== undefined ? input.focus() : null;
  };

  handleSubmit = () => {
    console.info("Form Submitted");
  };

  getSyncErrors = field => {
    const { meta, syncErrorList } = this.props;
    return meta.hasOwnProperty(field) ? (
      syncErrorList[field] ? (
        <div className="feedback">
          <small className="text-danger">
            <FontAwesomeIcon icon="exclamation-circle" className="mr-1" />
            {syncErrorList[field]}
          </small>
        </div>
      ) : null
    ) : null;
  };

  getAsyncErrors = field => {
    const { asyncErrorList } = this.props;
    return typeof asyncErrorList === "object" ? (
      asyncErrorList.hasOwnProperty(field) ? (
        <div className="feedback">
          <small className="text-danger">
            <FontAwesomeIcon icon="exclamation-circle" className="mr-1" />
            {asyncErrorList[field]}
          </small>
        </div>
      ) : null
    ) : null;
  };

  getClassList = field => {
    const { asyncValidating } = this.props;
    return typeof asyncValidating === "string" && asyncValidating === field
      ? " validating position-relative"
      : "";
  };

  render() {
    const { handleSubmit, pristine, reset, submitting } = this.props;
    return (
      <Fragment>
        <div className="d-flex flex-row justify-content-center" size="sm">
          <Card className="col-sm-12 col-md-8 col-lg-6 p-0 mt-3">
            <CardHeader>Redux Form</CardHeader>
            <CardBody>
              <Form onSubmit={handleSubmit(this.handleSubmit)}>
                <div className="row m-0">
                  <div className="col-xs-12 col-sm-6 p-0 col-sm-right">
                    <FormGroup className="has-float-label">
                      <Field
                        className="form-control"
                        component="input"
                        type="text"
                        name="firstName"
                        placeholder=""
                        required="required"
                      />
                      <Label htmlFor="firstName" onClick={this.focusInput}>
                        First name
                      </Label>
                      {this.getSyncErrors("firstName")}
                    </FormGroup>
                  </div>
                  <div className="col-xs-12 col-sm-6 p-0 col-sm-left">
                    <FormGroup className="has-float-label">
                      <Field
                        className="form-control"
                        component="input"
                        type="text"
                        name="lastName"
                        placeholder=""
                        required="required"
                      />
                      <Label htmlFor="lastName" onClick={this.focusInput}>
                        Last name
                      </Label>
                      {this.getSyncErrors("lastName")}
                    </FormGroup>
                  </div>
                </div>
                <div className="row m-0">
                  <div className="col-xs-12 col-sm-6 p-0 col-sm-right">
                    <FormGroup
                      className={"has-float-label" + this.getClassList("email")}
                    >
                      <Field
                        className="form-control"
                        component="input"
                        type="email"
                        name="email"
                        placeholder=""
                        required="required"
                      />
                      <Label htmlFor="email" onClick={this.focusInput}>
                        Email
                      </Label>
                      {this.getSyncErrors("email")}
                      {this.getAsyncErrors("email")}
                    </FormGroup>
                  </div>
                  <div className="col-xs-12 col-sm-6 p-0 col-sm-left">
                    <FormGroup className="has-float-label">
                      <Field
                        className="form-control"
                        component="input"
                        type="text"
                        name="phone"
                        placeholder=""
                        required="required"
                      />
                      <Label htmlFor="phone" onClick={this.focusInput}>
                        Phone
                      </Label>
                      {this.getSyncErrors("phone")}
                    </FormGroup>
                  </div>
                </div>
                <div className="row m-0">
                  <div className="col-xs-12 col-sm-6 p-0 col-sm-right">
                    <FormGroup className="has-float-label">
                      <Field
                        component="input"
                        type="date"
                        name="birthdate"
                        className="form-control label-floated"
                        required="required"
                      />
                      <Label for="birthdate">Birthdate</Label>
                      {this.getSyncErrors("birthdate")}
                    </FormGroup>
                  </div>
                  <div className="col-xs-12 col-sm-6 p-0 col-sm-left">
                    <FormGroup className="has-float-label">
                      <Field
                        component="select"
                        name="color"
                        className="form-control custom-select"
                        required="required"
                      >
                        <option value="" disabled>
                          -- Select --
                        </option>
                        <option value="red">Red</option>
                        <option value="green">Green</option>
                        <option value="blue">Blue</option>
                      </Field>
                      <Label for="color">Color</Label>
                      {this.getSyncErrors("color")}
                    </FormGroup>
                  </div>
                </div>
                <FormGroup className="custom-control custom-switch">
                  <Field
                    className="custom-control-input"
                    component="input"
                    type="checkbox"
                    name="employed"
                    id="employed"
                    value="false"
                  />
                  <Label className="custom-control-label" for="employed">
                    Employed
                  </Label>
                  {this.getSyncErrors("employed")}
                </FormGroup>
                <FormGroup className="has-float-label">
                  <Field
                    component="textarea"
                    className="form-control"
                    name="notes"
                    placeholder=""
                  />
                  <Label for="notes" onClick={this.focusInput}>
                    Notes
                  </Label>
                  {this.getSyncErrors("notes")}
                </FormGroup>
                <Button
                  size="sm"
                  className="float-right"
                  type="submit"
                  disabled={pristine || submitting}
                >
                  Submit
                </Button>
                <Button
                  size="sm"
                  outline
                  className="float-right mr-2"
                  type="button"
                  disabled={pristine || submitting}
                  onClick={reset}
                >
                  Clear
                </Button>
                <Button
                  size="sm"
                  className="float-left mr-2"
                  type="button"
                  onClick={() => {
                    this.props.loadDefaults(data);
                  }}
                >
                  Load Defaults
                </Button>
              </Form>
            </CardBody>
          </Card>
        </div>
        <ReduxFormViewer form="form" />
      </Fragment>
    );
  }
}

export default connect(
  state => {
    return {
      // initialValues: state.myreducer.data,
      meta: getFormMeta("form")(state),
      syncErrorList: getFormSyncErrors("form")(state),
      asyncErrorList: getFormAsyncErrors("form")(state)
    };
  },
  dispatch => {
    return {
      loadDefaults: data => {
        // dispatch(load(data));
        dispatch(initialize("form", data));
      }
    };
  }
)(
  reduxForm({
    form: "form",
    validate: validate,
    asyncValidate: asyncValidate,
    asyncChangeFields: ["email"],
    asyncBlurFields: ["email"]
  })(ReduxForm)
);
