import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getFormValues } from "redux-form";
import Prism from "prismjs";
import ReactHTMLPraser from "react-html-parser";
import unescape from "lodash/unescape";

class ReduxFormViewer extends Component {
  static propTypes = {
    values: PropTypes.object
  };

  render() {
    let code = Prism.highlight(
      unescape(JSON.stringify(this.props.values, null, 2)),
      Prism.languages.javascript
    ).replace(/\n/g, "</br>");

    return (
      <div className="d-flex flex-row justify-content-center" size="sm">
        <div className="col-sm-12 col-md-8 col-lg-6 p-0 my-3">
          <pre className="language-json m-0">{ReactHTMLPraser(code)}</pre>
        </div>
      </div>
    );
  }
}

export default connect((state, ownProps) => {
  return {
    values: getFormValues(ownProps.form)(state)
  };
})(ReduxFormViewer);
