import { combineReducers, createStore } from "redux";
import { reducer as formReducer } from "redux-form";
import myReducer from "./reducers/myReducer";

export const store = createStore(
  combineReducers({
    myreducer: myReducer,
    form: formReducer
  }),
  window.__REDUX_DEVTOOLS_EXTENSION__ &&
    window.__REDUX_DEVTOOLS_EXTENSION__({
      trace: true
    })
);
