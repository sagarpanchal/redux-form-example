const initState = {};

const myReducer = (state = initState, action) => {
  switch (action.type) {
    case "LOAD":
      return {
        data: action.data
      };
    default:
      return state;
  }
};

export default myReducer;
