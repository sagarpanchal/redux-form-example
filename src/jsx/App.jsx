import React, { Component } from "react";
import "./libraries/FontAwesome";
import { Container } from "reactstrap";
import ReduxForm from "./components/ReduxForm";
// import ExampleComponent from "./components/ExampleComponent";

export default class App extends Component {
  render() {
    return (
      <Container>
        <ReduxForm />
      </Container>
    );
  }
}
